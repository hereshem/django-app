from django.db import models

# Create your models here.

class Blog(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField(max_length=1000)
    category = models.CharField(max_length=250)
    author = models.CharField(max_length=250)
    created = models.DateTimeField("Created")
    modified = models.DateTimeField("Modified")

    def __str__(self):
        return self.title
