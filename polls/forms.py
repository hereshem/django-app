from django import forms
from .models import Choice


class PostChoiceForm(forms.ModelForm):

    class Meta:
        model = Choice
        fields = ["choice_text"]


