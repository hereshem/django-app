from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('<int:question_id>', views.detail),
    path('<int:question_id>/vote', views.vote),
    path('<int:question_id>/add_choice', views.add_choice),

]

