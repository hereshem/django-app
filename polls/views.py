from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse, HttpResponseRedirect

from polls.forms import PostChoiceForm, Choice
from .models import Question

# Create your views here.

def index(request):
    items = Question.objects.order_by('-created')

    # html = []
    # for item in items:
    #     html.append("Question is %s <br>" % (item.question_text))
    # return HttpResponse("".join(html))

    return render(request, "polls/index.html",
                  {"title" : "Question List"
                      , "items" : items}
                  )


def detail(request, question_id):
    question = Question.objects.get(pk=question_id) # pk = primary key
    forms = PostChoiceForm()
    data = {"question":question, "forms":forms}
    return render(request, "polls/detail.html", data)


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    choice_id = request.POST["choice"]

    selected_choice = question.choice_set.get(pk=choice_id)

    selected_choice.vote += 1
    selected_choice.save()

    return HttpResponseRedirect("/polls/" + str(question_id))


def add_choice(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    form = PostChoiceForm(request.POST)
    choice = form.save(commit=False)
    choice.question_id = question
    choice.save()

    return HttpResponseRedirect("/polls/" + str(question_id))